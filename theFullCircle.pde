/**
 * Rotates the PI / 3 base section to form a full hexagon, appending 
 */
LED[] theFullCircle() {
  PVector center = new PVector(centerX, centerY);
  ArrayList<LED> leds = new ArrayList<LED>(Arrays.asList(baseSextant));
  // Copy the sextant to make a circle
  for (int i = 1; i < 6; i++) {
    // The angle by which to rotate this copy of the LEDs
    float angle = i * PI / 3;
    for (LED led : baseSextant) {
      leds.add(led.rotatedCopy(center, angle, i * 2));
    }
  }
  return (LED[])leds.toArray(new LED[leds.size()]);
}