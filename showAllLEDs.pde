void showAllLEDs() {
  float diameter = LED_RADIUS * 2;
  for (LED led : leds) {
   PVector p = led.getPosition();
   stroke(getColour(led.getStripNumber()));
   fill(getColour(led.getStripNumber()));
   ellipse(p.x, p.y, diameter, diameter);
  }
}

int getColour(int stripNumber) {
  if (stripNumber % 2 == 0) {
    return color(200, 200, 0);
  } else {
    return color(0, 0, 255);
  }
}