/**
 * An LED has a Cartesian position vector, a PixelPusher strip number, and an index within that strip.
 */
public class LED {

  /**
   * The strip number this LED is part of.
   */
  protected int stripNumber;
  
  /**
   * The index of the LED within its array.
   */
  protected int index;
  
  /**
   * The colour of the LED
   */
  protected color colour;
  
  /**
   * The physical position of the LED.
   */
  protected PVector position;

  protected LED(int stripNumber, int index, float x, float y) {
    this.stripNumber = stripNumber;
    this.index = index;
    this.position = new PVector(x, y);
  }

  public PVector getPosition() {
    return position;
  }

  /**
   * Get a copy of the LED by rotating it a certain angle, and incrementing its strip number. 
   */
  public LED rotatedCopy(PVector center, float angle, int stripIncrement) {
    
    LED led = new LED(this.stripNumber + stripIncrement, this.index, this.position.x, this.position.y);
    
    // Rotate by angle around the center
    led.position.sub(center);
    led.position.rotate(angle);
    led.position.add(center);
    
    return led;
  }
  
  public int getStripNumber() {
    return stripNumber;
  }
  
  public String toString() {
    return "(" + (int)this.position.x + ", " + (int)this.position.y + ") strip: " + this.stripNumber + " index: " + this.index;
  }
  
  public void setColour(color c) {
    this.colour = c;
  }

  /**
   * Draw the LED on the screen
   */
  public void draw() {
    fill(this.colour);
    ellipse(this.position.x + CANVAS_WIDTH, this.position.y, LED_RADIUS, LED_RADIUS);
  }
  
  /**
   * Light the pixelPusher LED
   */
  public void pushPixel() {
    if (observer.hasStrips) {
      try {
        registry.getStrips().get(this.stripNumber).setPixel(this.colour, this.index);
      } catch (ArrayIndexOutOfBoundsException e) {
        e.printStackTrace();
      }
    }
  }
}