/**
 * Display positional about the LED at the given coordinates.
 */
void showLEDInfo(int x, int y) {
  LED led = ledAt(x, y);
  fill(255);
  rect(CANVAS_WIDTH, CANVAS_HEIGHT, CANVAS_WIDTH, 100);
  if (null != led) {
    fill(0);
    textFont(createFont("Arial", 14, true));
    text(led.toString(), CANVAS_WIDTH + 10, CANVAS_HEIGHT + 20);
  }
}

/**
 * Find the LED, if any, under the given coordinates.
 * @param x
 * @param y
 * @return
 */
LED ledAt(int x, int y) {
  PVector position = new PVector(x - CANVAS_WIDTH, y);
  for (LED led : leds) {
    if (led.position.dist(position) <= LED_RADIUS) {
      return led;
    }
  }
  return null;
}