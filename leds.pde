import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;
import processing.video.*;

import com.heroicrobot.dropbit.registry.*;
import com.heroicrobot.dropbit.devices.pixelpusher.Pixel;
import com.heroicrobot.dropbit.devices.pixelpusher.Strip;

DeviceRegistry registry;
PusherObserver observer;

final int CANVAS_WIDTH = 500;
final int CANVAS_HEIGHT = 500;

/**
 * The radius of each LED, for the screen.
 */
final int LED_RADIUS = 4;

LED[] leds;

Movie movie;

PImage pixelImage;

public void setup() {
  
  colorMode(RGB, 255, 255, 255, 100);
  
  leds = theFullCircle();
  
  size(1000, 600);

  registry = new DeviceRegistry();
  observer = new PusherObserver();
  registry.addObserver(observer);

  // Draw a background rectangle behind the LEDs
  fill(color(10, 10, 10));
  rect(CANVAS_WIDTH, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
  
  noStroke();
  
  loopMovie(sketchPath("sample.mov"));
}

public void loopMovie(String filePath) {
  movie = new Movie(this, filePath);
  movie.loop();
}

public void playMovie(String filePath, int hOffset, int vOffset) {
  
}

public void draw() {
  
//  showAllLEDs();
//  showAlignment();

  image(movie, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
 // loadPixels();
  for (LED led : leds) {
    led.setColour(get((int)led.position.x, (int)led.position.y));
    led.draw();
    led.pushPixel();
  }


}

void movieEvent(Movie m) {
  m.read();
}

/**
 * Show the LED's info when the mouse is moved over it.
 */
public void mouseMoved() {
  showLEDInfo(mouseX, mouseY);
}